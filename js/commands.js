if (annyang) {
  // Defining the variable "command" that will store all the voice commands for the site
  var commands = {

    // Top navigation bar voice commands
    '(go) (move) (to) (the) clothing (page)': function() {
      window.location = "/clothing.html";
    },

    '(go) (move) (to) (the) shoes (and) accessories (page)': function() {
      window.location = "/shoes.html";
    },

    '(go) (move) (to) (the) about (page)': function() {
      window.location = "/about.html";
    },

    '(go) (move) (to) (the) help (page)': function() {
      window.location = "/help.html";
    },

    // Navigation for header icons (account/login and shopping bag)
    '(go) (to) (the) (shopping) bag (page)': function() {
      window.location = "/bag.html";
    },

    '(go) (to) (the) (sign) (log) in (page)': function() {
      window.location = "/login-register.html";
    },

    '(go) (to) (the) register (an) (a) (new) account (page)': function() {
      window.location = "/login-register.html";
    },

    'sign up': function() {
      window.location = "/login-register.html";
    },

    '(go) (to) (the) (my) account (page)': function() {
      window.location = "/account.html";
    },

    // Navigation from about page to contact page
    '(go) (to) (the) contact (us) (company) (page)': function() {
      window.location = "/contact.html";
    },

    // Go to checkout page from shopping bag page
    '(go) (to) (the) (secure) checkout (page)': function() {
      window.location = "/checkout.html"
    },

    // Open item page on clothing page voice command - would usually lead to each individual item page for
    // each product but for this it only leads to one product item page.
    '(open) (go) (to) (the) pink (stripey) (knitted) jumper (page)': function() {
      window.location = "/item.html"
    },

    // Email Company voice command on  contact page
    '(send) email (to) (the) (topsy) (turvy) (company)': function() {
      window.location.href = "mailto:example@email.co.uk";
    },

    // In-page navigation (scrolling)
    'scroll down (to) (the) (bottom)': function() {
      window.scroll(0, window.innerHeight / 1);
    },

    'scroll up (to) (the) (top)': function() {
      window.scroll(0, 0);
    },

  };

  // Add commands to annyang
  annyang.addCommands(commands);

  // Start listening
  annyang.start();
}
