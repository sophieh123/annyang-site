if (annyang) {
  // Defining the variable "command" that will store all the voice commands for the site
  var commands = {

    // Home voice command
    '(go) (back) (to) (the) home (page)': function() {
      window.location = "/index.html";
    },

    // Go to order confirmation page from checkout page
    '(place) (complete) (the) order': function() {
      window.location = "/order.html";
    },

    // Continue shopping command (opens clothing page initially)
    '(continue) shopping': function() {
      window.locayion = "/clothing.html";
    },

    // In-page navigation (scrolling)
    'scroll down (to) (the) (bottom)': function() {
      window.scroll(0, window.innerHeight / 1);
    },

    'scroll up (to) (the) (top)': function() {
      window.scroll(0, 0);
    },

  };

  // Add commands to annyang
  annyang.addCommands(commands);

  // Start listening
  annyang.start();
}
